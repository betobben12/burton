
import $ from 'jquery';
import 'bootstrap';
import 'bootstrap-slider';


import "../scss/app.scss";

(function() {
  var requestAnimationFrame = window.requestAnimationFrame 
    || window.mozRequestAnimationFrame 
    || window.webkitRequestAnimationFrame 
    || window.msRequestAnimationFrame;

  window.requestAnimationFrame = requestAnimationFrame;
})();

$(function() { 
 //#region cargar video
  let $video = $("#video");
  let video = $video.get(0);
  let duration = video.duration;
  let currentTime = 0;
 //#endregio



  //representar elemento
  let $canvas = $("#preview");
  let canvas = $canvas.get(0) 
  canvas.width = video.width;
  canvas.height = video.height;
  let context = canvas.getContext("2d");

  let $tracksContent = $("#tracks-content");
  let data = [
    {
      id: 1,
      type:"text",
      name: "Track 1",
      x: 100,
      y: 100,
      width:100,
      height: 100,
      font: "lora",
      size: 14,
      color: "#ffffff",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis corrupti accusantium sapiente quibusdam dolorem porro officiis quam rerum corporis illum iste incidunt eligendi quae commodi, reprehenderit perferendis deleniti, quas aliquid.",
      start: 0,
      end: 4.5,
    },
    {
      id: 2,
      type:"image",
      name: "Track 2",
      x: 200,
      y: 200,
      width:100,
      height: 200,
      image:"/asset/image/stickers/emoji-sadness.png",
      start: 4.5,
      end: 9.95,
    }
  ];

  $tracksContent.html("");

  $(data).each(function (key, value) {
    trackAdd(value);
  });

  let $playBtn = $("#play-btn");
  let $slideMaster = $("#slider-master");
 

  //#region eventos
  $playBtn.on("click", function (e) {

    if (video.paused) {
      video.play();
      drawLoop();
    }
    else {
      video.pause();
    }

  });

  $slideMaster.slider({
    min:0,
    max: duration,
    step: 0.1,
    precision: 6,
    value:0,
    tooltip:'hide'
  })

  video.addEventListener('play', function(){
    $("#play-btn").html("<i class=\"fas fa-pause\"></i>");
  })

  video.addEventListener('pause', function(){
    $("#play-btn").html("<i class=\"fas fa-play\"></i>");
  })

  video.addEventListener('ended', function () {
    video.currentTime = 0;
  })

  video.addEventListener('seeked', function () {
    draw();
  })

  $slideMaster.on('change' , function(e) {
    video.currentTime = $slideMaster.slider('getValue');
  });
  //#endregion

  function trackAdd (element) {
    //contenedor principal
    let $container = $("<div>");
    $container.appendTo($tracksContent);
    $container.attr("id", "track-", + element.id);
    $container.addClass("track");

    //etiqueta
    let $label = $("<div>");
    $label.appendTo($conteiner);
    $label.addClass("track-label");

    let $text = $("<div>");
    $text.appendTo($label);
    $text.addClass("text");
    $text.html(element.name);

    //botones
    let $buttons = $("<div>");
    $buttons.appendTo($label);
    $buttons.addClass(buttons);

    let $edithBtn = $("<buttons>");
    $edithBtn.appendTo($buttons);
    $edithBtn.addClass("btn btn-sm btn-dark edit-btn");
    $edithBtn.html("<i class=\"fas fa-pen\"></i>");

    let $deleteBtn = $("<buttons>");
    $deleteBtn.appendTo($buttons);
    $deleteBtn.addClass("btn btn-sm btn-dark delete-btn");
    $deleteBtn.html("<i class=\"fas fa-trash\"></i>");

    //taimeline
    let $timeline = $("<div>");
    $timeline.appendTo($conteiner);
    $timeline.addClass(buttons);

    let $slider = $("<input>");
    $slider.appendTo($timeline);
    $slider.attr("name", "track-" + element.id);
    $slider.attr("type", 'text');

    $slider.slider({ 
      min: 0,
      max: duration,
      range: true,
      step: 0,
      presicion: 6,
      value: [element.start, element.end],
      tooltip: "hide",
    })
     

    function drawLoop () {
    requestAnimationFrame(drawLoop);
    
    if (video.ended || video.paused) {
      return;
    }

    draw();
  }

  /*
  *representa el video con sus elementos dentro del canvas
  */ 
  function draw() {
    requestAnimationFrame(draw);
    // si el video esta parado o se ha acabado no hagas nada y sal de aquí
    if (video.ended || video.paused){
     return;
    }

    currentTime = video.currentTime;
    
    $slideMaster.slider('setValue', currentTime);

    // pinta el video en el canvas
    context.drawImage(video, 0, 0, video.width, video.height);
  }
  
});